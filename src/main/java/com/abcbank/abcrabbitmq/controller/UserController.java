package com.abcbank.abcrabbitmq.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.abcbank.abcrabbitmq.entities.User;
import com.abcbank.abcrabbitmq.producer.RabbitMqJsonProducer;

@RestController
@RequestMapping("/api/v1")
public class UserController {
    private RabbitMqJsonProducer jsonProducer;

    public UserController(RabbitMqJsonProducer jsonProducer) {
        this.jsonProducer = jsonProducer;
    }

    @PostMapping("/publish")
    public ResponseEntity<String> sendJsonMessage(@RequestBody User user) {
        jsonProducer.sendMessage(user);
        return ResponseEntity.ok("Json Message sent to RabbitMq");
    }
}
