package com.abcbank.abcrabbitmq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import com.abcbank.abcrabbitmq.entities.User;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RabbitMqConsumer {

    @RabbitListener(queues = {"${rabbitmq.queue.name}"})
    public void consume(String message) {
        log.info("Received message -> {}", message);
    }

    @RabbitListener(queues = {"${rabbitmq.queue.json.name}"})
    public void consumeJson(User user) {
        log.info("Received json message -> {}", user.toString());
    }
}
