package com.abcbank.abcrabbitmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AbcRabbitMqApplication {

	public static void main(String[] args) {
		SpringApplication.run(AbcRabbitMqApplication.class, args);
	}
}

